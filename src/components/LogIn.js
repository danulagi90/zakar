import React, { Component } from 'react'
import 'bootstrap/dist/css/bootstrap.min.css'
import Logo from '../image/logo.png'

import '../css/app.css'

export default class LogIn extends Component {
  render() {
    return (
      <div className= "LogIn">
        <div className= "imageLogin mt-5"> 
          <img className="image-login" src={Logo}/>
        </div>

        <div className="input-username mb-3">
            <text className = "username"> Username : </text>
         <input type="text" className="form-control input" placeholder="Username" aria-label="Recipient's username" aria-describedby="basic-addon2"/>
        </div>
        <div className="input-password mb-3">
            <text className = "password"> Password : </text>
         <input type="text" className="form-control input" placeholder="Password" aria-label="Recipient's password" aria-describedby="basic-addon2"/>
        </div>
        <div className = "squareSignup">
          <button className = "btn btn-outline-info">Log In.</button>
          <span>
            <text className="linkSignup">Doesn't have account :</text>
            <a className="linkSignup"> Sign Up </a>
          </span>
        </div>
      </div>
    )
  }
}
