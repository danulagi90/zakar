import React, { Component } from 'react'
import 'bootstrap/dist/css/bootstrap.min.css'
import '../css/app.css'

export default class ItemList extends Component {
  render() {

  const lists = this.props.ItemList.map((list,index) => {
    return (
      <li key= {index} className="list-group-item d-flex justify-content-between align-items-center pt-2 pb-2">
        {list.id}. {list.title}
        <span className="spanBtn"> 
          <button className="btn btn-primary mr-1 pt-0 pb-0">edit</button>
          <button className="btn btn-danger pt-0 pb-0" onClick={() => this.props.deleteList(index,list)}>delete</button>
        </span>
      </li>
    )
  })

    return (
        <div className="itemList">
          <ul className="list-group">
            {lists}
          </ul>
      </div>
    )
  }
}
