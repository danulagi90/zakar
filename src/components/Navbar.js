import React, { Component } from 'react'
import 'bootstrap/dist/css/bootstrap.min.css'
import Logo from '../image/logo.png'
import '../css/app.css'

export default class Navbar extends Component {
  render() {
    return (
      <div className= "NavBar mt-3">
        <nav className="navbar navbar-light bg-light justify-content-between mb-3">
          <a className="navbar-brand"> <img className="navbar-logo" src={Logo}/> </a>

          <a className="burger-toggle">
            <button className="navbar-toggler-icon"></button>
          </a>
        </nav>
      </div>
    )
  }
}
