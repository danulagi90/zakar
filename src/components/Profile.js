import React, { Component } from 'react'
import Navbar from './Navbar'
import Tarmogoyf from '../image/tarmogoyf.jpg'
import '../css/app.css'

export default class Profile extends Component {
  render() {
    return (
      <div className="profile-Page">
        <Navbar/>

        <div className= "imageProfile mb-3"> 
          <img className="image-user" src={Tarmogoyf}/>
          <h2>Here's Your Profile To-Do-List!</h2>
        </div>

        <div className= "container">
          <form>
            <div class="form-group row">
              <label for="inputName" class="col-sm-2 col-form-label">Name</label>
              <div class="col-sm-10">
                <input type="Name" class="form-control" id="inputName" placeholder="Name"/>
              </div>
            </div>
            <div class="form-group row">
              <label for="inputEmail3" class="col-sm-2 col-form-label">Email</label>
              <div class="col-sm-10">
                <input type="email" class="form-control" id="inputEmail3" placeholder="Email"/>
              </div>
            </div>
            <div class="form-group row">
              <label for="inputPassword3" class="col-sm-2 col-form-label">Password</label>
              <div class="col-sm-10">
                <input type="password" class="form-control" id="inputPassword3" placeholder="Password"/>
              </div>
            </div>

            <div class="form-group row">
              <div class="col-sm-10">
                <button type="submit" class="btn btn-primary">Back</button>
                <button type="submit" class="btn btn-primary ml-5">Exit</button>
              </div>
            </div>
          </form>

        </div>
    </div>
    )
  }
}
