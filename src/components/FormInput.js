import React, { Component } from 'react'
import 'bootstrap/dist/css/bootstrap.min.css'
import '../css/app.css'

export default class FormInput extends Component {
  render() {
    return (
      <div className="input-group mb-3">
         <input type="text" className="form-control" placeholder="Insert your schedule today" aria-label="Recipient's username" aria-describedby="basic-addon2"/>
            <div className="input-group-append">
              <button className="btn btn-outline-secondary" type="button"> + add </button>
            </div>
      </div>
    )
  }
}
