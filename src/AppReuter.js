import React, { Component } from 'react'
import {BrowserRouter as Router, Route, Link} from 'react-router-dom'
import ComponentOne from './components/App'
import addItem from './components/addItem'
import App from './App'

export default class AppReuter extends Component {
  render() {
    return (
      <div>
        <h2>This is Your Router</h2>
          <Link to = "/app">Ini Link</Link>
          <Link to = "/add-Item">+ add</Link>
          <Route path="/app" component= {ComponentOne}/>
          <Route path="/addItem" component= {addItem}/>
      </div>
    )
  }
}
