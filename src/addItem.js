import React, { Component } from 'react'

export default class addItem extends Component {
    state = {
        input :"",
        datamasuk:"",
    }    

    onChange = e => {
        
        this.setState({
            datamasuk :e.target.value,
        })
    }

    catchMe = () => {
        this.setState({
            input: this.state.datamasuk
        })
    }


  render() {
    return (
      <div>
        <input onChange={this.onChange}/>
        <button onClick = {this.catchMe}>+ Add! </button>
        <h2>Hello {this.state.input}</h2>
      </div>
    )
  }
}

