import React, { Component } from 'react'
import 'bootstrap/dist/css/bootstrap.min.css'
import Navbar from './components/Navbar'
import FormInput from './components/FormInput'
import ItemList from './components/ItemList'
import Calender from './components/Calender'
import LogIn from './components/LogIn'
import axios from 'axios';

import './css/app.css'

export default class App extends Component {

  state = {
    listTodo : []
  }

  componentDidMount() {
    axios.get('https://jsonplaceholder.typicode.com/todos?_limit=10')
      .then(res => this.setState({
        listTodo: res.data
      }))
  }

  deleteList = (id,val) => {
    axios.delete(`https://jsonplaceholder.typicode.com/todos/${id}`)
    .then(res => {
      if(res){
        var array = [...this.state.listTodo];
        var index = array.indexOf(val);
        if (index !== -1) {
          array.splice(index, 1);
          this.setState({listTodo: array});
          console.log(this.state.listTodo)
        }
      }
    })
    
  }

  render() {
    console.log(this.state.listTodo)
    return (
      
      <div className="container">
        <Navbar/>
        <Calender/>
        <FormInput/>
        <ItemList ItemList= {this.state.listTodo} deleteList={this.deleteList} />
      </div>
    )
  }
}